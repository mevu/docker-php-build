#!/bin/bash

# build & deployment deps
apt-get update && apt-get install -y \
        git \
        zlib1g-dev \
        ruby2.1-dev \
        rubygems \
        build-essential \
    --no-install-recommends && rm -r /var/lib/apt/lists/*

# install deploy tool & deps
gem install dpl json nokogiri aws-sdk-v1

# install php extensions
docker-php-ext-install pdo_mysql bcmath mbstring zip

# install composer
curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer